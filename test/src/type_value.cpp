#include <nlc/meta/traits.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc::meta;

static_assert(is_greater<4, 2>);
static_assert(!is_greater<3, 3>);
static_assert(!is_greater<3, 5>);
static_assert(is_less<2, 4>);
static_assert(!is_less<2, 2>);
static_assert(!is_less<5, 2>);
static_assert(is_equal<3, 3>);
static_assert(!is_equal<3, 5>);

static_assert(is_empty_type<Value<3>>);

static_assert(value<3> == value<3>);
static_assert(value<3> == 3);
static_assert(value<3> != value<4>);
static_assert(value<3> != 4);
static_assert(value<3> < value<4>);
static_assert(value<0> == value<'\0'>);

static_assert(type<int> == type<int>);
static_assert(type<float> != type<int>);
static_assert(!(type<int> == type<float>));
static_assert(!(type<int> != type<int>));

int main() { return 0; }
