#include <nlc/meta/basics.hpp>
#include <nlc/meta/enable_if.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc;
using namespace nlc::meta;

template<typename T> struct is_int { static constexpr auto value = false; };
template<> struct is_int<int> { static constexpr auto value = true; };

template<typename T, nlc::enable_if<is_int<T>::value> = 0> auto test_enable_if_1(int) {
    return Value<true> {};
}
template<typename T> auto test_enable_if_1(float) { return Value<false> {}; }
template<typename T, typename = nlc::enable_if<is_int<T>::value>> auto test_enable_if_2(int) {
    return Value<true> {};
}
template<typename T> auto test_enable_if_2(float) { return Value<false> {}; }

static_assert(is_same<decltype(test_enable_if_1<int>(0)), Value<true>>);
static_assert(is_same<decltype(test_enable_if_1<char>(0)), Value<false>>);
static_assert(is_same<decltype(test_enable_if_2<int>(0)), Value<true>>);
static_assert(is_same<decltype(test_enable_if_2<char>(0)), Value<false>>);

auto main() -> int {}

