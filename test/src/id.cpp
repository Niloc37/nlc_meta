#include <nlc/meta/id.hpp>
#include <nlc/meta/is_same.hpp>

#include "../utils.hpp"

using namespace nlc::meta;

struct Ad {
    using underlying_t = int;
};
using A = id<Ad>;

struct Bd {
    using underlying_t = int;

    static constexpr auto default_value = 0;
};
using B = id<Bd>;

struct Cd {
    using underlying_t = int;

    static constexpr auto invalid_value = 0;
};
using C = id<Cd>;

struct Dd {
    using underlying_t = int;

    static constexpr auto invalid_value = 0;
    static constexpr auto default_value = 1;
};
struct D : public id<Dd, D> {
    using id<Dd, D>::id;
    auto patate() { return 3; }
};

static_assert(is_same<Ad::underlying_t, int, A::underlying_t>);
static_assert(is_same<Ad, A::descriptor_t>);
static_assert(is_same<A, A::Self>);
static_assert(is_same<decltype(A::invalid()), Error>);
// static_assert_fails_to_compile(NO_DEFAULT_CONSTRUCTOR_A, A());

static_assert(is_same<Bd::underlying_t, int, B::underlying_t>);
static_assert(is_same<Bd, B::descriptor_t>);
static_assert(is_same<B, B::Self>);
static_assert(is_same<decltype(B::invalid()), Error>);
static auto test_B() { assert(B {}.value() == Bd::default_value); }

static_assert(is_same<Cd::underlying_t, int, C::underlying_t>);
static_assert(is_same<Cd, C::descriptor_t>);
static_assert(is_same<C, C::Self>);
static_assert(is_same<decltype(C::invalid()), C>);
// static_assert_fails_to_compile(NO_DEFAULT_CONSTRUCTOR_C, C());
static auto test_C() { assert(C::invalid().value() == Cd::invalid_value); }

static_assert(is_same<Dd::underlying_t, int, D::underlying_t>);
static_assert(is_same<Dd, D::descriptor_t>);
static_assert(is_same<D, D::Self>);
static_assert(is_same<decltype(D::invalid()), D>);
static auto test_D() {
    auto d = D();
    assert(d.value() == Dd::default_value);
    assert(d.isValid());
    d = D::invalid();
    assert(d.value() == Dd::invalid_value);
    assert(!d.isValid());
    assert(d.patate() == 3);
}

int main() {
    // test_A();
    test_B();
    test_C();
    test_D();
    return 0;
}
