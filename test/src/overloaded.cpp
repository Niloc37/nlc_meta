#include <nlc/meta/overloaded.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc::meta;

auto overloaded_test = overloaded { [](int) { return value<0>; }, [](double) { return value<1>; } };
static_assert(overloaded_test(3) == value<0>);
static_assert(overloaded_test(3.2) == value<1>);

auto main() -> int {}

