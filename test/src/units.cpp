#include <nlc/meta/is_same.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/meta/units.hpp>

#include "../utils.hpp"

using namespace nlc::meta;

static_assert(is_same<impl::simplify<pair<list<int, char, float>, list<char, short>>>,
                      pair<list<int, float>, list<short>>>);
static_assert(impl::is_equivalent_dim<pair<list<int, float>, list<char, short>>,
                                      pair<list<float, int>, list<short, char>>>);
static_assert(is_trivially_destructible<meters<float>>);
static_assert(is_trivially_copyable<meters<float>>);

static auto d = meters<float>(1.5f);
static_assert_fails_to_compile(d = seconds<float>(2))

      [[maybe_unused]] static float f;
static_assert_fails_to_compile(f = seconds<float>(2))

      using t1 = unit<int, pair<list<int, double>, list<char, float, short>>>;
using t2       = unit<int, pair<list<double, int>, list<float, char, short>>>;
using t3       = unit<int, pair<list<double, int>, list<char, short>>>;
static_assert(is_equivalent<t1, t2>);

static auto f1(t1 v) { return 2 * v.v; }
static auto f2(t2 v) { return 3 * v.v; }

int main() {
    assert(d == 1.5f);
    // d = seconds<float>(3);

    auto d_int = meters<int>(0);
    assert(d_int == 0);
    d_int = static_cast<decltype(d_int)>(d);
    assert(d_int == 1);

    auto t = seconds<float>(2);

    auto s = d / t;
    static_assert(is_equivalent<decltype(s), metersPerSecond<float>>);

    auto s2 = 2.f * s;
    static_assert(is_equivalent<decltype(s2), metersPerSecond<float>>);

    auto s3 = s / t * unit<float, pair<list<pixel_t>, list<>>>(3) / meters<float>(2);
    static_assert(
          is_equivalent<decltype(s3), unit<float, pair<list<pixel_t>, list<second_t, second_t>>>>);

    auto v1 = t1(1);
    auto v2 = t2(2);

    assert(f1(v1) == 2);
    assert(f1(v2) == 4);
    assert(f2(v1) == 3);
    assert(f2(v2) == 6);

    return 0;
}
