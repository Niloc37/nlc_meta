#include <cassert>
#include <iostream>

#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/numeric_limits.hpp>

using namespace nlc::meta;

template<typename T> auto test() {
    auto m = limits<T>::min;
    auto M = limits<T>::max;
    // std::cout << m << ", " << M << std::endl;
    // std::cout << static_cast<T>(m - 1) << ", " << static_cast<T>(M + 1) << " : " std::endl;
    static_assert(is_same<T, decltype(m)>);
    static_assert(is_same<T, decltype(M)>);
    [[maybe_unused]] auto m2 = static_cast<T>(m - static_cast<T>(1));
    assert(m2 >= m);
    [[maybe_unused]] auto M2 = static_cast<T>(M + static_cast<T>(1));
    assert(M2 <= M);
}

auto main() -> int {
    test<char>();
    test<signed char>();
    test<unsigned char>();
    test<short>();
    test<unsigned short>();
    test<int>();
    test<unsigned int>();
    test<long>();
    test<unsigned long>();
    test<long long>();
    test<unsigned long long>();
}

