#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/traits.hpp>

using namespace nlc::meta;

enum class Enum {};

struct Struct final {};

int main() { return 0; }

// is_integral ///////////////////////////////////////////////////////////////

static_assert(!is_integral<void>);
static_assert(!is_integral<Enum>);
static_assert(!is_integral<Struct>);
static_assert(is_integral<bool>);
static_assert(is_integral<char>);
static_assert(is_integral<signed char>);
static_assert(is_integral<unsigned char>);
static_assert(is_integral<char16_t>);
static_assert(is_integral<char32_t>);
static_assert(is_integral<wchar_t>);
static_assert(is_integral<short>);
static_assert(is_integral<unsigned short>);
static_assert(is_integral<int>);
static_assert(is_integral<unsigned int>);
static_assert(is_integral<long>);
static_assert(is_integral<unsigned long>);
static_assert(is_integral<long long>);
static_assert(is_integral<unsigned long long>);
static_assert(!is_integral<float>);
static_assert(!is_integral<double>);
static_assert(!is_integral<long double>);

// is_unsigned_integer ///////////////////////////////////////////////////////

static_assert(!is_unsigned_integer<void>);
static_assert(!is_unsigned_integer<Enum>);
static_assert(!is_unsigned_integer<Struct>);
static_assert(!is_unsigned_integer<bool>);
static_assert(!is_unsigned_integer<char>);
static_assert(!is_unsigned_integer<signed char>);
static_assert(is_unsigned_integer<unsigned char>);
static_assert(!is_unsigned_integer<char16_t>);
static_assert(!is_unsigned_integer<char32_t>);
static_assert(!is_unsigned_integer<wchar_t>);
static_assert(!is_unsigned_integer<short>);
static_assert(is_unsigned_integer<unsigned short>);
static_assert(!is_unsigned_integer<int>);
static_assert(is_unsigned_integer<unsigned int>);
static_assert(!is_unsigned_integer<long>);
static_assert(is_unsigned_integer<unsigned long>);
static_assert(!is_unsigned_integer<long long>);
static_assert(is_unsigned_integer<unsigned long long>);
static_assert(!is_unsigned_integer<float>);
static_assert(!is_unsigned_integer<double>);
static_assert(!is_unsigned_integer<long double>);

// is_signed_integer /////////////////////////////////////////////////////////

static_assert(!is_signed_integer<void>);
static_assert(!is_signed_integer<Enum>);
static_assert(!is_signed_integer<Struct>);
static_assert(!is_signed_integer<bool>);
static_assert(!is_signed_integer<char>);
static_assert(is_signed_integer<signed char>);
static_assert(!is_signed_integer<unsigned char>);
static_assert(!is_signed_integer<char16_t>);
static_assert(!is_signed_integer<char32_t>);
static_assert(!is_signed_integer<wchar_t>);
static_assert(is_signed_integer<short>);
static_assert(!is_signed_integer<unsigned short>);
static_assert(is_signed_integer<int>);
static_assert(!is_signed_integer<unsigned int>);
static_assert(is_signed_integer<long>);
static_assert(!is_signed_integer<unsigned long>);
static_assert(is_signed_integer<long long>);
static_assert(!is_signed_integer<unsigned long long>);
static_assert(!is_signed_integer<float>);
static_assert(!is_signed_integer<double>);
static_assert(!is_signed_integer<long double>);

// is_integer ////////////////////////////////////////////////////////////////

static_assert(!is_integer<void>);
static_assert(!is_integer<Enum>);
static_assert(!is_integer<Struct>);
static_assert(!is_integer<bool>);
static_assert(!is_integer<char>);
static_assert(is_integer<signed char>);
static_assert(is_integer<unsigned char>);
static_assert(!is_integer<char16_t>);
static_assert(!is_integer<char32_t>);
static_assert(!is_integer<wchar_t>);
static_assert(is_integer<short>);
static_assert(is_integer<unsigned short>);
static_assert(is_integer<int>);
static_assert(is_integer<unsigned int>);
static_assert(is_integer<long>);
static_assert(is_integer<unsigned long>);
static_assert(is_integer<long long>);
static_assert(is_integer<unsigned long long>);
static_assert(!is_integer<float>);
static_assert(!is_integer<double>);
static_assert(!is_integer<long double>);

// is_floating ///////////////////////////////////////////////////////////////

static_assert(!is_floating<void>);
static_assert(!is_floating<Enum>);
static_assert(!is_floating<Struct>);
static_assert(!is_floating<bool>);
static_assert(!is_floating<char>);
static_assert(!is_floating<signed char>);
static_assert(!is_floating<unsigned char>);
static_assert(!is_floating<char16_t>);
static_assert(!is_floating<char32_t>);
static_assert(!is_floating<wchar_t>);
static_assert(!is_floating<short>);
static_assert(!is_floating<unsigned short>);
static_assert(!is_floating<int>);
static_assert(!is_floating<unsigned int>);
static_assert(!is_floating<long>);
static_assert(!is_floating<unsigned long>);
static_assert(!is_floating<long long>);
static_assert(!is_floating<unsigned long long>);
static_assert(is_floating<float>);
static_assert(is_floating<double>);
static_assert(is_floating<long double>);

// volatile //////////////////////////////////////////////////////////////////

static_assert(is_same<rm_volatile<int>, int>);
static_assert(is_same<rm_volatile<int const>, int const>);
static_assert(is_same<rm_volatile<int volatile>, int>);
static_assert(is_same<rm_volatile<int const volatile>, int const>);

static_assert(!is_volatile<int>);
static_assert(is_volatile<int volatile>);
static_assert(!is_volatile<int const>);
static_assert(is_volatile<int const volatile>);
static_assert(!is_volatile<int const &>);
static_assert(is_volatile<int volatile const &>);

static_assert(is_same<add_volatile<int>, int volatile>);
static_assert(is_same<add_volatile<int volatile>, int volatile>);

// reference /////////////////////////////////////////////////////////////////

static_assert(is_same<rm_ref<int>, int>);
static_assert(is_same<rm_ref<int &>, int>);
static_assert(is_same<rm_ref<int &&>, int>);
static_assert(is_same<rm_ref<int const>, int const>);
static_assert(is_same<rm_ref<int const &>, int const>);
static_assert(is_same<rm_ref<int const &&>, int const>);

static_assert(!is_lref<int>);
static_assert(is_lref<int &>);
static_assert(is_lref<int const &>);
static_assert(is_lref<int volatile &>);
static_assert(is_lref<int volatile const &>);
static_assert(!is_lref<int &&>);
static_assert(!is_rref<int>);
static_assert(!is_rref<int &>);
static_assert(is_rref<int &&>);
static_assert(is_rref<int const &&>);
static_assert(is_rref<int volatile &&>);
static_assert(is_rref<int volatile const &&>);

static_assert(is_same<add_lref<int>, int &>);
static_assert(is_same<add_lref<int &>, int &>);
static_assert(is_same<add_lref<int &&>, int &>);
static_assert(is_same<add_lref<int const>, int const &>);
static_assert(is_same<add_lref<int const &>, int const &>);
static_assert(is_same<add_lref<int const &&>, int const &>);
static_assert(is_same<add_rref<int>, int &&>);
static_assert(is_same<add_rref<int &>, int &>);
static_assert(is_same<add_rref<int &&>, int &&>);
static_assert(is_same<add_rref<int const>, int const &&>);
static_assert(is_same<add_rref<int const &>, int const &>);
static_assert(is_same<add_rref<int const &&>, int const &&>);

// const /////////////////////////////////////////////////////////////////////

static_assert(!is_const<int>);
static_assert(is_const<int const>);
static_assert(is_const<int const &>);
static_assert(!is_const<int &>);
static_assert(is_const<int volatile const>);
static_assert(!is_const<int volatile>);

static_assert(is_same<rm_const<int>, int>);
static_assert(is_same<rm_const<int const>, int>);

static_assert(is_same<add_const<int>, int const>);
static_assert(is_same<add_const<int const>, int const>);

// const ref and const volatile //////////////////////////////////////////////

static_assert(is_same<rm_const_ref<int>, int>);
static_assert(is_same<rm_const_ref<int &>, int>);
static_assert(is_same<rm_const_ref<int &&>, int>);
static_assert(is_same<rm_const_ref<int const>, int>);
static_assert(is_same<rm_const_ref<int const &>, int>);
static_assert(is_same<add_const_ref<int>, int const &>);
static_assert(is_same<add_const_ref<int &>, int const &>);
static_assert(is_same<add_const_ref<int &&>, int const &>);
static_assert(is_same<add_const_ref<int const>, int const &>);
static_assert(is_same<add_const_ref<int const &>, int const &>);

static_assert(is_same<rm_const_volatile<int>, int>);
static_assert(is_same<rm_const_volatile<int &>, int &>);
static_assert(is_same<rm_const_volatile<int const &>, int const &>);
static_assert(is_same<rm_const_volatile<int volatile>, int>);
static_assert(is_same<rm_const_volatile<int volatile &&>, int volatile &&>);
static_assert(is_same<rm_const_volatile<int const volatile>, int>);
static_assert(is_same<rm_const_volatile<int const volatile &>, int volatile const &>);
static_assert(is_same<rm_const_volatile<int const volatile &&>, int volatile const &&>);

// has_virtual_destructor ////////////////////////////////////////////////////

namespace v_descr {
struct A {};
struct B {
    virtual ~B() = default;
};
struct C : public A {};
struct D : public B {};
struct E : private A {};
struct F : private B {};

static_assert(!has_virtual_destructor<A>);
static_assert(has_virtual_destructor<B>);
static_assert(!has_virtual_destructor<C>);
static_assert(has_virtual_destructor<D>);
static_assert(!has_virtual_destructor<E>);
static_assert(has_virtual_destructor<F>);
}  // namespace v_descr

// is array //////////////////////////////////////////////////////////////////

namespace array {
static constexpr auto           a   = "test";
static constexpr decltype(auto) b   = "test";
constexpr int                   c[] = { 3, 5, 6 };
constexpr int *                 d   = nullptr;

static_assert(!is_array<decltype(a)>);
static_assert(!is_array<decltype(b)>);
static_assert(is_array<rm_ref<decltype(b)>>);
static_assert(is_array<decltype(c)>);
static_assert(!is_array<decltype(d)>);

static_assert(array_len<rm_ref<decltype(b)>> == 5);
static_assert(array_len<decltype(c)> == 3);
}  // namespace array

// is empty type /////////////////////////////////////////////////////////////

namespace empty_type {
struct A {};
struct B {
    int a;
};

static_assert(is_empty_type<A>);
static_assert(!is_empty_type<B>);
}  // namespace empty_type
