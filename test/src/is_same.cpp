#include <nlc/meta/is_same.hpp>

using namespace nlc::meta;

static_assert(is_same<int, int>);
static_assert(!is_same<float, int>);
static_assert(is_same<int, int, int, int>);
static_assert(!is_same<float, int, int, int>);
static_assert(!is_same<int, int, float, int>);
static_assert(!is_same<int, int, int, float>);

auto main() -> int {}

