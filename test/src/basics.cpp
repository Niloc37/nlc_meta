#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc::meta;

// identity //////////////////////////////////////////////////////////////////

static_assert(type<identity<int>> == type<int>);

// forward ///////////////////////////////////////////////////////////////////

struct LValue {};
struct RValue {};

[[maybe_unused]] static auto lvalue = 43;

static auto test_valueness(int const &) { return LValue {}; }
static auto test_valueness(int &&) { return RValue {}; }

template<typename T> auto forward_foo(T && arg) { return test_valueness(nlc::forward<T>(arg)); }

static_assert(is_same<decltype(forward_foo(lvalue)), LValue>);
static_assert(is_same<decltype(forward_foo(42)), RValue>);

// move //////////////////////////////////////////////////////////////////////

static_assert(is_same<decltype(test_valueness(lvalue)), LValue>);
static_assert(is_same<decltype(test_valueness(nlc::move(lvalue))), RValue>);

// main //////////////////////////////////////////////////////////////////////

int main() { return 0; }
