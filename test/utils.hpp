#pragma once

#include <iostream>

#define assert(cond)                               \
    do {                                           \
        if (cond) {                                \
        } else {                                   \
            std::cerr << "\"" #cond "\" failed\n"; \
            exit(1);                               \
        }                                          \
    } while (false)

#if (defined(__GNUC__) || defined(__GNUG__)) && !defined(__clang__)
#    define static_assert_fails_to_compile(EXPR)                                                  \
        template<typename T> auto test_compilation_##__LINE__(float)->Value<true>;                \
        template<typename T> auto test_compilation_##__LINE__(int)->decltype(EXPR, value<false>); \
        static_assert(rm_const_ref<decltype(test_compilation_##__LINE__<int>(0))>::value);
#else
#    define static_assert_fails_to_compile(EXPR)
#endif
