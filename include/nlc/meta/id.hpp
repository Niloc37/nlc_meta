#pragma once

#include "enable_if.hpp"
#include "logic.hpp"
#include "traits.hpp"

namespace nlc::meta {

//-------------------------------------------------------------------------

namespace impl {

    //-------------------------------------------------------------------------

#define DEFINE_ACCESSOR_HELPERS(value_name)                                                            \
    template<typename Description_t>                                                                   \
    constexpr auto is_##value_name##_defined_impl(int)->decltype(Description_t::value_name, bool {}) { \
        return true;                                                                                   \
    }                                                                                                  \
    template<typename Description_t> constexpr auto is_##value_name##_defined_impl(float)->bool {      \
        return false;                                                                                  \
    }                                                                                                  \
    template<typename Description_t> constexpr auto is_##value_name##_defined() {                      \
        return is_##value_name##_defined_impl<Description_t>(0);                                       \
    }                                                                                                  \
    template<typename Description_t> constexpr auto get_##value_name() {                               \
        if constexpr (is_##value_name##_defined<Description_t>())                                      \
            return Description_t::value_name;                                                          \
        else                                                                                           \
            return error_;                                                                             \
    }

    DEFINE_ACCESSOR_HELPERS(default_value)
    DEFINE_ACCESSOR_HELPERS(invalid_value)
    DEFINE_ACCESSOR_HELPERS(is_copyable)
    DEFINE_ACCESSOR_HELPERS(has_increment)

#undef DEFINE_ACCESSOR_HELPERS

    //-------------------------------------------------------------------------

}  // namespace impl

//-------------------------------------------------------------------------

/* struct Description_t final {
 *     using underlying_t = uint;
 *     static constexpr underlying_t invalid_value = 0;
 *     static constexpr underlying_t default_value = invalid_value;
 *     // static constexpr bool has_increment = true; // false by default
 * }
 */

template<typename Descr_t, typename Self_t = Default> class id {
  public:
    using Self         = if_default<Self_t, id>;
    using descriptor_t = Descr_t;
    using underlying_t = typename descriptor_t::underlying_t;

    static constexpr auto has_default_value = impl::is_default_value_defined<descriptor_t>();
    static constexpr auto has_invalid_value = impl::is_invalid_value_defined<descriptor_t>();

    [[nodiscard]] static constexpr auto default_value() {
        if constexpr (!has_default_value)
            return error_;
        else
            return impl::get_default_value<descriptor_t>();
    }

    [[nodiscard]] static constexpr auto invalid_value() {
        if constexpr (!has_invalid_value)
            return error_;
        else
            return impl::get_invalid_value<descriptor_t>();
    }

  public:
    constexpr explicit id(underlying_t const & value)
          : _value(value) {}

    constexpr id()
          : _value(default_value()) {}

    constexpr id(id const & rhs) = default;
    constexpr auto operator=(id const & rhs) -> id & = default;

  public:
    [[nodiscard]] constexpr decltype(auto) value() const { return (_value); }
    [[nodiscard]] constexpr decltype(auto) value() { return (_value); }

  public:
    static constexpr auto invalid() {
        if constexpr (has_invalid_value)
            return Self(invalid_value());
        else
            return error_;
    }

    [[nodiscard]] constexpr auto isValid() const -> bool {
        if constexpr (has_invalid_value)
            return _value != invalid_value();
        else
            return true;
    }
    [[nodiscard]] constexpr auto isInvalid() const -> bool {
        if constexpr (has_invalid_value)
            return _value == invalid_value();
        else
            return false;
    }

  private:
    underlying_t _value;
};

//-------------------------------------------------------------------------

#define OP(op)                                                                             \
    template<typename D, typename T>                                                       \
    [[nodiscard]] constexpr auto operator op(id<D, T> const & lhs, id<D, T> const & rhs) { \
        return lhs.value() op rhs.value();                                                 \
    }

OP(==)
OP(!=)
OP(<)
OP(>)
OP(<=)
OP(>=)

#undef OP

//-------------------------------------------------------------------------

#define OP(op)                                                                                                              \
    template<typename D, typename T, typename Integer, enable_if<impl::get_has_increment<D>() && is_integral<Integer>> = 0> \
    [[nodiscard]] constexpr auto operator op(id<D, T> const & lhs, Integer rhs) {                                           \
        return id<D, T>(lhs.value() op rhs);                                                                                \
    }                                                                                                                       \
    template<typename D, typename T, typename Integer, enable_if<impl::get_has_increment<D>() && is_integral<Integer>> = 0> \
    [[nodiscard]] constexpr auto operator op(Integer lhs, id<D, T> const & rhs) {                                           \
        return id<D, T>(lhs op rhs.value());                                                                                \
    }                                                                                                                       \
    template<typename D, typename T, enable_if<impl::get_has_increment<D>()> = 0>                                           \
    constexpr auto operator op##op(id<D, T> & v)->id<D, T> {                                                                \
        const auto res = v;                                                                                                 \
        v.value() op##op;                                                                                                   \
        return res;                                                                                                         \
    }                                                                                                                       \
    template<typename D, typename T, enable_if<impl::get_has_increment<D>()> = 0>                                           \
    constexpr auto operator op##op(id<D, T> & v, int)->id<D, T> & {                                                         \
        op##op v.value();                                                                                                   \
        return v;                                                                                                           \
    }                                                                                                                       \
    template<typename D, typename T, typename Integer, enable_if<impl::get_has_increment<D>()> = 0>                         \
    inline auto operator op##=(id<D, T> & lhs, Integer rhs)->id<D, T> & {                                                   \
        lhs.value() op## = rhs;                                                                                             \
        return lhs;                                                                                                         \
    }

OP(+)
OP(-)

#undef OP

//-------------------------------------------------------------------------

}  // namespace nlc::meta
