#pragma once

#include "../list.hpp"
#include "../type_value.hpp"

#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<long long int b, long long int e> struct range_impl {
        static_assert(b < e);
        using type = push_front<Value<b>, typename range_impl<b + 1, e>::type>;
    };
    template<long long int b> struct range_impl<b, b> { using type = list<>; };
}  // namespace impl
template<long long int b, long long int e> using range = typename impl::range_impl<b, e>::type;

}  // namespace nlc::meta

