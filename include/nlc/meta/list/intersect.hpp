#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "count.hpp"
#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L1, typename L2, typename Acc> struct intersect_impl;
    template<typename L2, typename Acc> struct intersect_impl<list<>, L2, Acc> {
        using type = Acc;
    };
    template<typename H, typename... T, typename L2, typename Acc>
    struct intersect_impl<list<H, T...>, L2, Acc> {
        using next_acc = if_t<is_greater<count<H, L2>, count<H, Acc>>, push_back<Acc, H>, Acc>;
        using type     = typename intersect_impl<list<T...>, L2, next_acc>::type;
    };
}  // namespace impl
template<typename L1, typename L2>
using intersect = typename impl::intersect_impl<L1, L2, list<>>::type;

}  // namespace nlc::meta

