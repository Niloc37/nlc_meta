#pragma once

#include "../list.hpp"
#include "../logic.hpp"
#include "../tags.hpp"

#include "get.hpp"
#include "head_tail.hpp"
#include "map.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... Ls> struct zip_impl {
        template<typename i> using multi_get  = list<get<i::value, Ls>...>;
        static constexpr auto first_list_size = head<list<Ls...>>::size;
        static constexpr bool is_input_valid  = ((Ls::size == first_list_size) && ...);
        using type = if_t<is_input_valid, map<multi_get, range<0, first_list_size>>, Error>;
    };
}  // namespace impl
template<typename... Ls> using zip = typename impl::zip_impl<Ls...>::type;

}  // namespace nlc::meta

