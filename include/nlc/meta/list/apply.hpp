#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L> struct apply_impl;
    template<typename... Ts> struct apply_impl<list<Ts...>> {
        template<template<typename...> typename F> using type = F<Ts...>;
    };
}  // namespace impl
template<template<typename...> typename F, typename L>
using apply = typename impl::apply_impl<L>::template type<F>;

}  // namespace nlc::meta

