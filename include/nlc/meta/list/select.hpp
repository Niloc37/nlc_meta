#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<template<typename> typename C, typename L, typename Acc> struct select_impl;
    template<template<typename> typename C, typename Acc> struct select_impl<C, list<>, Acc> {
        using type = Acc;
    };
    template<template<typename> typename C, typename H, typename... T, typename Acc>
    struct select_impl<C, list<H, T...>, Acc> {
        using next_acc = if_t<C<H>::value, push_back<Acc, H>, Acc>;
        using type     = typename select_impl<C, list<T...>, next_acc>::type;
    };
}  // namespace impl
template<template<typename> typename C, typename L>
using select = typename impl::select_impl<C, L, list<>>::type;

}  // namespace nlc::meta

