#pragma once

#include "../list.hpp"

#include "boxing.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L> struct for_each_impl;
    template<typename... Ts> struct for_each_impl<list<Ts...>> {
        template<typename F> static auto foo(F && f) { (f(box<Ts> {}), ...); }
    };
}  // namespace impl
template<typename L, typename F> auto for_each(F && f) {
    impl::for_each_impl<L>::foo(forward<F>(f));
}

}  // namespace nlc::meta

