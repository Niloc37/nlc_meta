#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... Ts> struct reverse_impl_aux;
    template<> struct reverse_impl_aux<> { using type = list<>; };
    template<typename T, typename... Ts> struct reverse_impl_aux<T, Ts...> {
        using type = push_back<typename reverse_impl_aux<Ts...>::type, T>;
    };

    template<typename T> struct reverse_impl;
    template<typename... Ts> struct reverse_impl<list<Ts...>> {
        using type = typename reverse_impl_aux<Ts...>::type;
    };
}  // namespace impl
template<typename L> using reverse = typename impl::reverse_impl<L>::type;

}  // namespace nlc::meta

