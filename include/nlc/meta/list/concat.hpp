#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... Ls> struct concat_impl;
    template<> struct concat_impl<> { using type = list<>; };
    template<typename... Ts> struct concat_impl<list<Ts...>> { using type = list<Ts...>; };
    template<typename... Ts1, typename... Ts2> struct concat_impl<list<Ts1...>, list<Ts2...>> {
        using type = list<Ts1..., Ts2...>;
    };
    template<typename L1, typename L2, typename... R> struct concat_impl<L1, L2, R...> {
        using type = typename concat_impl<typename concat_impl<L1, L2>::type, R...>::type;
    };
}  // namespace impl
template<typename... Ls> using concat = typename impl::concat_impl<Ls...>::type;

}  // namespace nlc::meta

