#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<template<typename> typename F, typename M> struct map_impl;
    template<template<typename> typename F, typename... Ts> struct map_impl<F, list<Ts...>> {
        using type = list<F<Ts>...>;
    };
}  // namespace impl
template<template<typename> typename F, typename M> using map = typename impl::map_impl<F, M>::type;

}  // namespace nlc::meta

