#pragma once

#include "../list.hpp"
#include "../logic.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L, typename Acc> struct remove_doublons_impl;
    template<typename Acc> struct remove_doublons_impl<list<>, Acc> { using type = Acc; };
    template<typename H, typename... T, typename Acc>
    struct remove_doublons_impl<list<H, T...>, Acc> {
        using type =
              typename remove_doublons_impl<list<T...>, if_t<contains<Acc, H>, Acc, push_back<Acc, H>>>::type;
    };
}  // namespace impl
template<typename L> using remove_doublons = typename impl::remove_doublons_impl<L, list<>>::type;

}  // namespace nlc::meta

