#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "count.hpp"
#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L1, typename L2, typename Res, typename Acc> struct substract_impl;
    template<typename L2, typename Res, typename Acc> struct substract_impl<list<>, L2, Res, Acc> {
        using type = Res;
    };
    template<typename H, typename... T, typename L2, typename Res, typename Acc>
    struct substract_impl<list<H, T...>, L2, Res, Acc> {
        using type = if_t<is_greater<count<H, L2>, count<H, Acc>>,
                          typename substract_impl<list<T...>, L2, Res, push_back<Acc, H>>::type,
                          typename substract_impl<list<T...>, L2, push_back<Res, H>, Acc>::type>;
    };
}  // namespace impl
template<typename L1, typename L2>
using substract = typename impl::substract_impl<L1, L2, list<>, list<>>::type;

}  // namespace nlc::meta

