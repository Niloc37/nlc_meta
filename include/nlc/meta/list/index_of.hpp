#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename T, typename L> struct index_of_impl;
    template<typename T, typename F, typename... R> struct index_of_impl<T, list<F, R...>> {
        static constexpr auto value =
              static_cast<comptime_int>(1 + index_of_impl<T, list<R...>>::value);
    };
    template<typename T, typename... R> struct index_of_impl<T, list<T, R...>> {
        static constexpr auto value = static_cast<comptime_int>(0);
    };
}  // namespace impl
template<typename T, typename L> inline constexpr auto index_of = impl::index_of_impl<T, L>::value;

}  // namespace nlc::meta

