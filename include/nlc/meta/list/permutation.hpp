#pragma once

#include "../list.hpp"

#include "count.hpp"
#include "get.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L, typename P> struct permutation_impl { using type = list<>; };
    template<typename... Ts> struct permutation_impl<list<Ts...>, list<>> { using type = list<>; };
    template<typename... Ts, auto... vs>
    struct permutation_impl<list<Ts...>, integral_list<vs...>> {
        using type = list<get<vs, list<Ts...>>...>;
    };
}  // namespace impl
template<typename L, typename P> using permutation = typename impl::permutation_impl<L, P>::type;

namespace impl {
    template<typename Acc, typename L1, typename L2> struct is_simple_permutation_impl;
    template<typename... Ts1, typename... Ts2>
    struct is_simple_permutation_impl<list<>, list<Ts1...>, list<Ts2...>> {
        static constexpr auto value = (sizeof...(Ts1) == sizeof...(Ts2));
    };
    template<typename H, typename... T, typename... Ts1, typename... Ts2>
    struct is_simple_permutation_impl<list<H, T...>, list<Ts1...>, list<Ts2...>> {
        static constexpr auto value =
              (count<H, list<Ts1...>> == count<H, list<Ts2...>>)
                    ? is_simple_permutation_impl<list<T...>, list<Ts1...>, list<Ts2...>>::value
                    : false;
    };
}  // namespace impl
template<typename L1, typename L2>
inline constexpr auto is_simple_permutation = impl::is_simple_permutation_impl<L1, L1, L2>::value;

}  // namespace nlc::meta

