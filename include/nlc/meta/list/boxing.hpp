#pragma once

#include "../type_value.hpp"

namespace nlc::meta {

namespace impl {
    template<typename T> struct box_impl { using type = Type<T>; };
    template<typename T> struct box_impl<Type<T>> { using type = Type<T>; };
    template<comptime_int v> struct box_impl<Value<v>> { using type = Value<v>; };
    template<typename T> struct unbox_impl { using type = T; };
    template<typename T> struct unbox_impl<Type<T>> { using type = T; };
}  // namespace impl
template<typename T> using box   = typename impl::box_impl<T>::type;
template<typename T> using unbox = typename impl::unbox_impl<T>::type;

}  // namespace nlc::meta

