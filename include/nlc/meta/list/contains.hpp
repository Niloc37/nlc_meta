#pragma once

#include "../basics.hpp"
#include "../is_same.hpp"
#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L, typename T> struct contains_impl;
    template<typename... Ts, typename T> struct contains_impl<list<Ts...>, T> {
        static constexpr auto value = (is_same<T, Ts> || ...);
    };
}  // namespace impl
template<typename L, typename T> inline constexpr auto contains = impl::contains_impl<L, T>::value;

}  // namespace nlc::meta

