#pragma once

#include "../basics.hpp"
#include "../is_same.hpp"
#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L, typename T> struct count_impl;
    template<typename... Ts, typename T> struct count_impl<list<Ts...>, T> {
        static constexpr auto value = static_cast<comptime_int>(((is_same<T, Ts> ? 1 : 0) + ... + 0));
    };
}  // namespace impl
template<typename T, typename L> inline constexpr auto count = impl::count_impl<L, T>::value;

}  // namespace nlc::meta

