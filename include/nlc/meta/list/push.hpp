#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename T, typename L> struct push_back_impl;
    template<typename T, typename... Ts> struct push_back_impl<T, list<Ts...>> {
        using type = list<Ts..., T>;
    };
    template<typename T, typename L> struct push_front_impl;
    template<typename T, typename... Ts> struct push_front_impl<T, list<Ts...>> {
        using type = list<T, Ts...>;
    };
}  // namespace impl
template<typename L, typename T> using push_back  = typename impl::push_back_impl<T, L>::type;
template<typename T, typename L> using push_front = typename impl::push_front_impl<T, L>::type;

}  // namespace nlc::meta

