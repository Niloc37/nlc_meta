#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... T> struct head_impl;
    template<typename H, typename... T> struct head_impl<list<H, T...>> { using type = H; };
    template<typename... T> struct tail_impl;
    template<typename H, typename... T> struct tail_impl<list<H, T...>> {
        using type = list<T...>;
    };
}  // namespace impl
template<typename T> using head = typename impl::head_impl<T>::type;
template<typename T> using tail = typename impl::tail_impl<T>::type;

}  // namespace nlc::meta

