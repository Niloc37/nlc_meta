#pragma once

#include "../list.hpp"
#include "../logic.hpp"

namespace nlc::meta {

namespace impl {
    template<comptime_int i, typename L> struct get_impl;
    template<comptime_int i> struct get_impl<i, list<>> { using type = Error; };
    template<comptime_int i, typename H, typename... T> struct get_impl<i, list<H, T...>> {
        using type = if_t<i == 0, H, typename get_impl<i - 1, list<T...>>::type>;
    };
}  // namespace impl
template<comptime_int i, typename L> using get = typename impl::get_impl<i, L>::type;

}  // namespace nlc::meta

