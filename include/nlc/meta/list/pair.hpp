#pragma once

#include "../list.hpp"

namespace nlc::meta {

template<typename T1, typename T2> using pair = list<T1, T2>;

namespace impl {
    template<typename T> struct first_impl;
    template<typename T1, typename T2> struct first_impl<pair<T1, T2>> { using type = T1; };
    template<typename T> struct second_impl;
    template<typename T1, typename T2> struct second_impl<pair<T1, T2>> { using type = T2; };
}  // namespace impl
template<typename T> using first  = typename impl::first_impl<T>::type;
template<typename T> using second = typename impl::second_impl<T>::type;

}  // namespace nlc::meta

