#pragma once

namespace nlc::meta {

template<typename T> struct limits;

template<> struct limits<char> {
    static constexpr char min = -128;
    static constexpr char max = 127;
};

template<> struct limits<signed char> {
    static constexpr signed char min = -128;
    static constexpr signed char max = 127;
};

template<> struct limits<unsigned char> {
    static constexpr unsigned char min = 0u;
    static constexpr unsigned char max = 255u;
};

template<> struct limits<short int> {
    static constexpr short int min = -32768;
    static constexpr short int max = 32767;
};

template<> struct limits<unsigned short int> {
    static constexpr unsigned short int min = 0u;
    static constexpr unsigned short int max = 65535u;
};

template<> struct limits<int> {
    static constexpr int min = -2147483648;
    static constexpr int max = 2147483647;
};

template<> struct limits<unsigned int> {
    static constexpr unsigned int min = 0u;
    static constexpr unsigned int max = 4294967295u;
};

template<> struct limits<long long int> {
    static constexpr long long int min = -9223372036854775807ll - 1ll;
    static constexpr long long int max = 9223372036854775807ll;
};

template<> struct limits<unsigned long long int> {
    static constexpr unsigned long long int min = 0ull;
    static constexpr unsigned long long int max = 18446744073709551615ull;
};

template<> struct limits<long int> {
    static constexpr long int min = sizeof(long int) == sizeof(int)  //
                                          ? limits<int>::min
                                          : limits<long long int>::min;
    static constexpr long int max = sizeof(long int) == sizeof(int)  //
                                          ? limits<int>::max
                                          : limits<long long int>::max;
};

template<> struct limits<unsigned long int> {
    static constexpr unsigned long int min = sizeof(long int) == sizeof(int)
                                                   ? limits<unsigned int>::min
                                                   : limits<unsigned long long int>::min;
    static constexpr unsigned long int max = sizeof(long int) == sizeof(int)
                                                   ? limits<unsigned int>::max
                                                   : limits<unsigned long long int>::max;
};

}  // namespace nlc::meta

