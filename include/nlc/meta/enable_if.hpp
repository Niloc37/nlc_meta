#pragma once

#include "tags.hpp"

namespace nlc {

/** enable_if is a concise helper for disabling functions with SFINAE, similarly
 * to what std::enable_if does.
 * usage :
 * template <typename T, meta::enable_if<meta::is_integral<T>> = 0> auto foo(T &&...args) {...}
 */
namespace impl {
    template<bool B, class T = int> struct enable_if_impl {};
    template<typename T> struct enable_if_impl<true, T> { using type = T; };
}  // namespace impl
template<bool B, class T = int> using enable_if = typename impl::enable_if_impl<B, T>::type;

}  // namespace nlc
