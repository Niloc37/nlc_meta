#pragma once

#include "list/concat.hpp"
#include "list/intersect.hpp"
#include "list/pair.hpp"
#include "list/permutation.hpp"
#include "list/substract.hpp"

#include "enable_if.hpp"
#include "list.hpp"
#include "logic.hpp"
#include "traits.hpp"

namespace nlc::meta {

// Dimension/////////////////////////////////////////////

namespace impl {

    template<typename Dim1, typename Dim2>
    inline constexpr auto is_equivalent_dim = is_simple_permutation<first<Dim1>, first<Dim2>> &&
                          is_simple_permutation<second<Dim1>, second<Dim2>>;

    // ------------------------------------------

    enum class unitOp { SUM, MULT, DIV };

    // ------------------------------------------

    template<unitOp op, typename Dim1, typename Dim2> struct Op { using type = Error; };

    // ------------------------------------------

    template<typename Dim> struct simplify_impl {
        using inter = intersect<first<Dim>, second<Dim>>;
        using type  = pair<substract<first<Dim>, inter>, substract<second<Dim>, inter>>;
    };
    template<typename Dim> using simplify = typename simplify_impl<Dim>::type;

    // ------------------------------------------

    template<typename Dim1, typename Dim2> struct Op<unitOp::SUM, Dim1, Dim2> {
        using type = if_t<is_equivalent_dim<Dim1, Dim2>, Dim1, Error>;
    };

    // ------------------------------------------

    template<typename Dim1, typename Dim2> struct Op<unitOp::MULT, Dim1, Dim2> {
        using type =
              simplify<pair<concat<first<Dim1>, first<Dim2>>, concat<second<Dim1>, second<Dim2>>>>;
    };

    // ------------------------------------------
    template<typename Dim1, typename Dim2> struct Op<unitOp::DIV, Dim1, Dim2> {
        using type =
              simplify<pair<concat<first<Dim1>, second<Dim2>>, concat<second<Dim1>, first<Dim2>>>>;
    };

    // ------------------------------------------
}  // namespace impl

// unit /////////////////////////////////////////////////

template<typename T, typename D> class unit final {
  public:
    using underlying = T;
    using dim        = D;

  public:
    unit()                       = delete;
    constexpr unit(unit &&)      = default;
    constexpr unit(unit const &) = default;
    auto operator=(unit &&) -> unit & = default;
    auto operator=(unit const &) -> unit & = default;

    constexpr unit(T const & val)
          : v(val) {}
    auto operator=(T const & val) -> unit & {
        v = val;
        return *this;
    }

    explicit constexpr operator T &() { return v; }
    explicit constexpr operator const T &() const { return v; }

    template<typename Q, typename E, enable_if<impl::is_equivalent_dim<D, E>> = 0>
    explicit constexpr operator unit<Q, E>() const {
        return unit<Q, E>(static_cast<Q>(v));
    }

    template<typename E, enable_if<impl::is_equivalent_dim<D, E>> = 0>
    constexpr operator unit<T, E>() const {
        return unit<T, E>(v);
    }

  public:
    T v;
};
template<typename T> unit(T &&) -> unit<rm_const_ref<T>, list<list<>, list<>>>;

// is_unit /////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_unit_impl : Value<false> {};
    template<typename T, typename D> struct is_unit_impl<unit<T, D>> : Value<true> {};
}  // namespace impl
template<typename T> inline constexpr auto is_unit = impl::is_unit_impl<T>::value;

// Standard operations /////////////////////////////////////////////////

namespace impl {
    template<typename U1, typename U2> struct is_equivalent {
        static constexpr auto value = false;
    };
    template<typename T, typename D1, typename D2> struct is_equivalent<unit<T, D1>, unit<T, D2>> {
        static constexpr auto value = is_equivalent_dim<D1, D2>;
    };
}  // namespace impl
template<typename U1, typename U2>
inline constexpr auto is_equivalent = impl::is_equivalent<U1, U2>::value;

#define define_op(op)                                                                                   \
    template<typename T, typename D1, typename D2>                                                      \
    [[nodiscard]] constexpr inline auto operator op(unit<T, D1> const & lhs, unit<T, D2> const & rhs) { \
        return static_cast<const T &>(lhs) op static_cast<const T &>(rhs);                              \
    }                                                                                                   \
    template<typename T, typename D, typename Q>                                                        \
    [[nodiscard]] constexpr inline auto operator op(unit<T, D> const & lhs, Q const & rhs) {            \
        return static_cast<const T &>(lhs) op rhs;                                                      \
    }                                                                                                   \
    template<typename T, typename D, typename Q>                                                        \
    [[nodiscard]] constexpr inline auto operator op(Q const & lhs, unit<T, D> const & rhs) {            \
        return lhs op static_cast<const T &>(rhs);                                                      \
    }

define_op(==) define_op(!=) define_op(<) define_op(>) define_op(<=) define_op(>=)
#undef define_op

// Standard operations /////////////////////////////////////////////////

#define define_op(op, OP)                                                                               \
    template<typename T, typename D1, typename D2>                                                      \
    [[nodiscard]] constexpr inline auto operator op(unit<T, D1> const & lhs, unit<T, D2> const & rhs) { \
        return unit<T, typename impl::Op<impl::unitOp::OP, D1, D2>::type>(                              \
              static_cast<const T &>(lhs) op static_cast<const T &>(rhs));                              \
    }

      define_op(+, SUM) define_op(-, SUM) define_op(*, MULT) define_op(/, DIV)
#undef define_op

            template<typename T, typename D, typename Q>
            [[nodiscard]] constexpr inline auto operator*(unit<T, D> const & lhs, Q const & rhs) {
    return unit<T, D>(static_cast<const T &>(lhs) * rhs);
}

template<typename T, typename D, typename Q>
[[nodiscard]] constexpr inline auto operator*(Q const & lhs, unit<T, D> const & rhs) {
    return unit<T, D>(lhs * static_cast<const T &>(rhs));
}

template<typename T, typename D, typename Q>
[[nodiscard]] constexpr inline auto operator/(unit<T, D> const & lhs, Q const & rhs) {
    return unit<T, D>(static_cast<const T &>(lhs) / rhs);
}

template<typename T, typename D, typename Q>
[[nodiscard]] constexpr inline auto operator/(Q const & lhs, unit<T, D> const & rhs) {
    return unit<T, list<second<D>, first<D>>>(lhs / static_cast<T const &>(rhs));
}

template<typename T, typename D1, typename D2>
constexpr inline auto operator+=(unit<T, D1> & lhs, unit<T, D2> const & rhs)
      -> unit<T, typename impl::Op<impl::unitOp::SUM, D1, D2>::type> & {
    static_cast<T &>(lhs) += static_cast<const T &>(rhs);
    return lhs;
}

template<typename T, typename D1, typename D2>
constexpr inline auto operator-=(unit<T, D1> & lhs, unit<T, D2> const & rhs)
      -> unit<T, typename impl::Op<impl::unitOp::SUM, D1, D2>::type> & {
    static_cast<T &>(lhs) -= static_cast<const T &>(rhs);
    return lhs;
}

template<typename T, typename D, typename Q>
constexpr inline auto operator*=(unit<T, D> & lhs, Q const & rhs) -> unit<T, D> & {
    static_cast<T &>(lhs) *= rhs;
    return lhs;
}

template<typename T, typename D, typename Q>
constexpr inline auto operator/=(unit<T, D> & lhs, Q const & rhs) -> unit<T, D> & {
    static_cast<T &>(lhs) /= rhs;
    return lhs;
}

// Standard unites /////////////////////////////////////////////////////

#define define_dimensions(name) \
    struct name##_t final {};   \
    template<typename T> using name##s = unit<T, pair<list<name##_t>, list<>>>

// ISO units
define_dimensions(meter);
define_dimensions(second);
define_dimensions(kilogram);
define_dimensions(ampere);
define_dimensions(kelvin);
define_dimensions(mole);
define_dimensions(candela);

// Specific unit
define_dimensions(pixel);
define_dimensions(byte);

#undef define_dimensions

// Compound units
template<typename T> using metersPerSecond = unit<T, pair<list<meter_t>, list<second_t>>>;
template<typename T> using hertz           = unit<T, pair<list<>, list<second_t>>>;
template<typename T>
using newtons = unit<T, pair<list<kilogram_t, meter_t>, list<second_t, second_t>>>;
}  // namespace nlc::meta

namespace nlc {

// unit_cast ///////////////////////////////////////////////////////////

template<template<typename> typename newUnit, typename T, typename D>
auto unit_cast(meta::unit<T, D> const & u) {
    return newUnit<T>(u.v);
}

template<typename T, typename D> auto rm_unit(meta::unit<T, D> const & u) -> T const & {
    return u.v;
}
template<typename T, typename D> auto rm_unit(meta::unit<T, D> & u) -> T & { return u.v; }

}  // namespace nlc
