#pragma once

#include "type_value.hpp"

namespace nlc::meta {

// -------------------------------------------

template<typename... T> struct list {
    static constexpr auto size = static_cast<comptime_int>(sizeof...(T));
};

// -------------------------------------------

template<typename L> using size = Value<L::size>;

// -------------------------------------------

template<auto... vs> using integral_list = list<Value<vs>...>;

}  // namespace nlc::meta
