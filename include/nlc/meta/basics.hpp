#pragma once

namespace nlc::meta {

// identity //////////////////////////////////////////////////////////////////

template<typename T> using identity = T;

}  // namespace nlc::meta

namespace nlc {

namespace meta_impl {  // avoid include over traits.hpp
    template<typename T> struct rm_ref_impl { using type = T; };
    template<typename T> struct rm_ref_impl<T &> : rm_ref_impl<T> {};
    template<typename T> struct rm_ref_impl<T &&> : rm_ref_impl<T> {};
    template<typename T> using rm_ref = typename rm_ref_impl<T>::type;
}  // namespace meta_impl

// forward ///////////////////////////////////////////////////////////////////

template<class T>[[nodiscard]] constexpr T && forward(meta_impl::rm_ref<T> & t) noexcept {
    return static_cast<T &&>(t);
}
template<class T>[[nodiscard]] constexpr T && forward(meta_impl::rm_ref<T> && t) noexcept {
    return static_cast<T &&>(t);
}
#define nlc_fwd(arg) ::nlc::forward<decltype(arg)>(arg)

// move //////////////////////////////////////////////////////////////////////

template<class T>[[nodiscard]] constexpr meta_impl::rm_ref<T> && move(T && t) noexcept {
    return static_cast<meta_impl::rm_ref<T> &&>(t);
}

// declval

template<typename T> extern const T declval;

}  // namespace nlc
