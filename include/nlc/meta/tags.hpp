#pragma once

namespace nlc::meta {

struct Default {};
constexpr auto default_ = Default {};
struct Nothing {};
constexpr auto nothing_ = Nothing {};
struct Error {
    Error() = default;
};
constexpr auto error_ = Error {};

}  // namespace nlc::meta

