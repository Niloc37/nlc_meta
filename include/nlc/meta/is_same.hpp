#pragma once

namespace nlc::meta {

namespace impl {
    template<typename... T> struct is_same_impl {};
    template<> struct is_same_impl<> { static constexpr auto value = true; };
    template<typename T> struct is_same_impl<T> { static constexpr auto value = true; };
    template<typename T1, typename T2> struct is_same_impl<T1, T2> {
        static constexpr auto value = false;
    };
    template<typename T> struct is_same_impl<T, T> { static constexpr auto value = true; };
    template<typename F, typename S, typename... Next> struct is_same_impl<F, S, Next...> {
        static constexpr auto value = is_same_impl<F, S>::value && is_same_impl<S, Next...>::value;
    };
}  // namespace impl
template<typename... T> inline constexpr auto is_same = impl::is_same_impl<T...>::value;

}  // namespace nlc::meta

