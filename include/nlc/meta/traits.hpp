#pragma once

#include "basics.hpp"

namespace nlc::meta {

#define set_true(name, type) \
    template<> struct name<type> { static constexpr auto value = true; }

// reference /////////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct rm_ref_impl { using type = T; };
    template<typename T> struct rm_ref_impl<T &> : rm_ref_impl<T> {};
    template<typename T> struct rm_ref_impl<T &&> : rm_ref_impl<T> {};
    template<typename T> struct is_rref_impl { static constexpr auto value = false; };
    template<typename T> struct is_rref_impl<T &&> { static constexpr auto value = true; };
    template<typename T> struct is_lref_impl { static constexpr auto value = false; };
    template<typename T> struct is_lref_impl<T &> { static constexpr auto value = true; };
}  // namespace impl

template<typename T> inline constexpr auto is_lref = impl::is_lref_impl<T>::value;
template<typename T> inline constexpr auto is_rref = impl::is_rref_impl<T>::value;
template<typename T> inline constexpr auto is_ref  = is_lref<T> || is_rref<T>;

template<typename T> using rm_ref   = typename impl::rm_ref_impl<T>::type;
template<typename T> using add_lref = T &;
template<typename T> using add_rref = T &&;

// pointer ///////////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_ptr { static constexpr auto value = false; };
    template<typename T> struct is_ptr<T *> { static constexpr auto value = true; };
}  // namespace impl
template<typename T> using is_ptr = typename impl::is_ptr<T>::value;

template<typename T> using add_ptr = T *;

// volatile //////////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_volatile_impl { static constexpr auto value = false; };
    template<typename T> struct is_volatile_impl<T volatile> {
        static constexpr auto value = true;
    };
    template<typename T> struct rm_volatile_impl { using type = T; };
    template<typename T> struct rm_volatile_impl<T volatile> : rm_volatile_impl<T> {};
}  // namespace impl

template<typename T> inline constexpr auto is_volatile = impl::is_volatile_impl<rm_ref<T>>::value;

template<typename T> using rm_volatile  = typename impl::rm_volatile_impl<T>::type;
template<typename T> using add_volatile = T volatile;

// const /////////////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct rm_const { using type = T; };
    template<typename T> struct rm_const<T const> : rm_const<T> {};
    template<typename T> struct is_const_impl { static constexpr auto value = false; };
    template<typename T> struct is_const_impl<T const> { static constexpr auto value = true; };
}  // namespace impl

template<typename T> inline constexpr auto is_const = impl::is_const_impl<rm_ref<T>>::value;

template<typename T> using rm_const  = typename impl::rm_const<T>::type;
template<typename T> using add_const = T const;

// const ref and const volatile //////////////////////////////////////////////

template<typename T> inline constexpr auto is_const_ref = is_const<T> && is_ref<T>;

template<typename T> using rm_const_ref  = rm_const<rm_ref<T>>;
template<typename T> using add_const_ref = rm_const_ref<T> const &;

template<typename T> inline constexpr auto is_const_volatile = is_const<T> && is_volatile<T>;
template<typename T> using rm_const_volatile                 = rm_volatile<rm_const<T>>;
template<typename T> using add_const_volatile                = T const volatile;

// has_virtual_destructor ////////////////////////////////////////////////////

template<typename T> inline constexpr auto has_virtual_destructor = __has_virtual_destructor(T);

// is array

namespace impl {
    template<typename T> struct is_array { static constexpr auto value = false; };
    template<typename T, auto size> struct is_array<T[size]> {
        static constexpr auto value = true;
    };
    template<typename T> struct array_len {};
    template<typename T, auto size> struct array_len<T[size]> {
        static constexpr auto value = size;
    };
}  // namespace impl
template<typename T> inline constexpr auto is_array  = impl::is_array<T>::value;
template<typename T> inline constexpr auto array_len = impl::array_len<T>::value;

// is empty type

template<typename T> inline constexpr auto is_empty_type = __is_empty(T);

// is trivially ...

template<typename T> inline constexpr auto is_trivially_copyable     = __is_trivially_copyable(T);
template<typename T> inline constexpr auto is_trivially_destructible = __has_trivial_destructor(T);

// is_enum

template<typename T> static constexpr auto is_enum = __is_enum(T);

// is_union

template<typename T> static constexpr auto is_union = __is_union(T);

// is_class

template<typename T> static constexpr auto is_class = __is_class(T);

// underlying_type

template<typename T> using underlying_type = __underlying_type(T);

// is_integral ///////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_integral_impl { static constexpr auto value = false; };
    set_true(is_integral_impl, bool);
    set_true(is_integral_impl, char);
    set_true(is_integral_impl, signed char);
    set_true(is_integral_impl, unsigned char);
    set_true(is_integral_impl, char16_t);
    set_true(is_integral_impl, char32_t);
    set_true(is_integral_impl, wchar_t);
    set_true(is_integral_impl, short);
    set_true(is_integral_impl, unsigned short);
    set_true(is_integral_impl, int);
    set_true(is_integral_impl, unsigned int);
    set_true(is_integral_impl, long);
    set_true(is_integral_impl, unsigned long);
    set_true(is_integral_impl, long long);
    set_true(is_integral_impl, unsigned long long);
}  // namespace impl
template<typename T>
inline constexpr auto is_integral = impl::is_integral_impl<rm_const_volatile<T>>::value;

// is_unsigned_integer ///////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_unsigned_integer_impl { static constexpr auto value = false; };
    set_true(is_unsigned_integer_impl, unsigned char);
    set_true(is_unsigned_integer_impl, unsigned short);
    set_true(is_unsigned_integer_impl, unsigned int);
    set_true(is_unsigned_integer_impl, unsigned long);
    set_true(is_unsigned_integer_impl, unsigned long long);
}  // namespace impl
template<typename T>
inline constexpr auto is_unsigned_integer =
      impl::is_unsigned_integer_impl<rm_const_volatile<T>>::value;

// is_signed_integer /////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_signed_integer_impl { static constexpr auto value = false; };
    set_true(is_signed_integer_impl, signed char);
    set_true(is_signed_integer_impl, short);
    set_true(is_signed_integer_impl, int);
    set_true(is_signed_integer_impl, long);
    set_true(is_signed_integer_impl, long long);
}  // namespace impl
template<typename T>
inline constexpr auto is_signed_integer = impl::is_signed_integer_impl<rm_const_volatile<T>>::value;

// is_integer ////////////////////////////////////////////////////////////////

template<typename T>
inline constexpr auto is_integer = impl::is_unsigned_integer_impl<rm_const_volatile<T>>::value ||
                                   impl::is_signed_integer_impl<rm_const_volatile<T>>::value;

// is_floating ///////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_floating_impl { static constexpr auto value = false; };
    set_true(is_floating_impl, float);
    set_true(is_floating_impl, double);
    set_true(is_floating_impl, long double);
}  // namespace impl
template<typename T> inline constexpr auto is_floating = impl::is_floating_impl<T>::value;

// is_arithmetic /////////////////////////////////////////////////////////////

template<typename T> inline constexpr auto is_arithmetic = is_integral<T> || is_floating<T>;

#undef set_true

}  // namespace nlc::meta
